const express = require("express");
const fs = require("fs").promises;
const path = require("path");

const app = express();
const port = 8032;

app.use(express.json());

app.post("/", (req, res) => {
  fs.mkdir(path.join(__dirname, req.body.dirname))
    .then(() => {
      let filespath = path.join(__dirname, req.body.dirname);
      let arrayoffiles = [];
      for (let val of req.body.files) {
        arrayoffiles.push(
          fs.writeFile(path.join(filespath, val), "hii mithup")
        );
      }
      return Promise.all(arrayoffiles);
    })
    .then(() => {
      res.setHeader("Context-Type", "application/json");
      res.setHeader("status", "success");
      res.json({ message: "directory and files are Created Succesfully" });
    })
    .catch((err) => {
      res.setHeader("Context-Type", "application/json");
      res.setHeader("status", "Failed");
      res.status(500);
      res.send({ message: "you get a some error man" });
      console.error(err);
    });
});

app.delete("/", (req, res) => {
  let arrayoffiles = req.body.files;
  let arrayofdeletedfiles = [];
  let undeletedfiles=[];
  let deletetedfile=[];
  fs.readdir(path.join(__dirname, req.body.dirname), "utf-8")
    .then((data) => {
      for (let file of arrayoffiles) {
        if (data.includes(file)) {
            deletetedfile.push(file);
          arrayofdeletedfiles.push(
            fs.unlink(path.join(__dirname, req.body.dirname, file))
          );
        }else{
            undeletedfiles.push(file)
        }
      }
 

      if(deletetedfile.length != arrayoffiles.length){
        res.setHeader("Context-Type", "application/json");
        res.setHeader("status", "success");
        res.json({ message: `FILES DELETED ${deletetedfile} not deletedfiles ${undeletedfiles}` });
      }

      if (deletetedfile.length == arrayoffiles.length) {
        res.setHeader("Context-Type", "application/json");
        res.setHeader("status", "success");
        res.json({ message: " files deleted Succesfully " });
      }
      return Promise.all(arrayofdeletedfiles);
    })
    .catch((err) => {
      res.setHeader("Context-Type", "application/json");
      res.setHeader("status", "Failed");
      res.status(500);
      res.send({ message: "you get a some error man" });
      console.error(err);
    });
});

app.listen(port, () => {
  console.log(`server is running on ${port}`);
});
